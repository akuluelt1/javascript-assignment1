/*
Nice work AK!  Your application functions great.  Your validations
work perfectly and the user can enter things as required.  There was
a couple of things missed:
The validate() function needed to be named validateForm()
I didn't see any JS comments - just HTML comments.
Otherwise, great work
18/20
*/

function loadProvinces(){
    var provArray=["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"];
    var output="<option value=''>-Select-</output>";
    for(index=0;index<provArray.length;index++){
        output+="<option value='"+provArray[index]+"'>"+provArray[index]+"</option>";
    }
    document.getElementById('cboProvince').innerHTML=output;
}

function validate(){
    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;
    var province = document.getElementById('cboProvince').value;

    if(name==""){
        alert("Please enter your Name");
        document.getElementById("txtName").focus();
        return false;
    }

    if(email==""){
        alert("Please enter an Email")
        document.getElementById("txtEmail").focus();
        return false;

    }

    if(province==""){
        alert('Please select a Province');
        document.getElementById('cboProvince').focus();
        return false;
    }

    alert('CONGRATULATIONS!!! You know what you are doing');
}
